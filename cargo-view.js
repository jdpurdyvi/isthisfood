// https://developer.mozilla.org/en-US/docs/Web/Events/DOMContentLoaded
document.addEventListener("DOMContentLoaded", function() {
    if (document.getElementById("myLineChart") != null) {
        load_cargo_view();
    }
});

function load_cargo_view() {

    var chart_data = JSON.parse(chart_json);
    var data = [];
    var labels = [];

	// console.log(chart_data);
	for (row of chart_data) {
	// Each row is an object
	// console.log(row);
		data.push(row.AirCargoTons);
		labels.push(moment(row.ReportPeriod).format('YYYY-MM'));
	}
	
	console.log(data);
	console.log(labels);

var timeFormat = 'YYYY-MM';

// http://www.chartjs.org/docs/latest/getting-started/usage.html
var ctx = document.getElementById("myLineChart");
var myLineChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: labels,
        datasets: [{
            label: 'Air Cargo Tons',
			data: data,
			fill: false,
			borderWidth: 2,
			borderColor: '#28a745',
			lineTension: 0.4
        }]
    },
	options: {
		scales: {
			xAxes: [{
				time: {
					format: timeFormat,
				}
			}]
		}
	}

});

}

<?php
/*
 * Nothing to fancy, just going to start building with a static page
 */
 
    if (!empty($_GET['page'])) {
        $page = $_GET['page']; // Switch later
    } else {
        $page = 'cargo-view';
        include_once('cargo-view.php');
        // Gives us $chart['json']
    }
?><!DOCTYPE HTML>
<html>
    <head>
        <!-- <link rel="icon" href="../../../../favicon.ico"> TODO FIND SANTA TAK -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- 3rd Party Libraries and Tools -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
        <script src="Chart.js"></script> <!-- Version 2.7.2 -->
        
        <script src="isthisfood.js?v=2"></script>
        <!-- <script src="chart.js?v=2"></script> --><!-- Quick Demo Script -->
        <script src="cargo-view.js?v=4"></script> <!-- Cargo View Logic -->
<?php
    switch ($page) {
        case 'site-data':
            break;
        case 'cargo-view':
        default:
            echo '
        <script type="text/javascript">
            var chart_json = \''.print_r($chart['json'], 1).'\'
        </script>';
            break;
    }
?>        
        <style>
/* Show it is fixed to the top */
body {
    min-height: 75rem;
    padding-top: 4.5rem;
}

.accesskey {
    text-decoration: underline;
}
        </style>
       <title>IsThisFood.net</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <a class="navbar-brand" href="/">IsThisFood</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item <?php echo $page=='cargo-view'?'active':'' ?>">
                        <a class="nav-link" href="/">Cargo View</a>
                    </li>
                    <li class="nav-item <?php echo $page=='site-data'?'active':'' ?>">
                        <a class="nav-link" href="/?page=site-data">Site Data</a>
                    </li>
                </ul>
            </div>
        </nav>
<?php
        switch ($page) {
            case 'site-data':
                include_once('site-data.php');
                break;
            case 'cargo-view':
            default:
                echo '
        <div class="container-fluid">
	    <div class="row">
		<div class="col-sm-12">
                <div class="card w-100 text-white bg-primary">
                    <div class="card-header">
                        <h3>Cargo View</h3>
                    </div>
            	    <div class="card-body">
                        <p class="card-text">Reporting Tool to explore Air Cargo Volume moving in and out of LAX from 2016-01-01 until 2018-03-31. Pulled from <cite title="Data.gov Source"><a class="text-white" href="http://data.gov"><u>Data.gov</u></a> on 2018-07-14</cite></p>
    		    </div>
		</div>
	        </div>
	    </div>
            <hr/>
            <div class="row">
                <div class="col-sm-12 col-md-7 col-lg-5">
                    <div class="container-fluid">
                        <div class="row">
                            '.$html['form'].'
                        </div>
                    </div>
		    <hr/>
		    <div class="container-fluid">
                        <div class="row">
                            '.$html['table'].'
                        </div>
                    </div>
		        </div>
			<div class="col-sm-12 col-md-5 col-lg-7">
		        <div class="card">
                    <div class="card-header">
                        <h4>Report Chart</h4>
                    </div>
                    <div class="card-body">
                        <canvas id="myLineChart"></canvas>
                    </div>
                    <div class="card-footer text-muted">
                        <p>Air Cargo Tons over a select period of dates.</p>
                    </div>
                </div>
            </div>
        </div>';
                break;
        }
?>
    </body>
</html>

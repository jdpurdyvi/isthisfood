<?php
/*
 * Start a list of all those sites that have been helpful
 */
?>
<div class="container-fluid">
    <div class="card-deck">
    <div class="card">
            <div class="card-body">
                <h3 class="card-title">Resources</h5>
                <ul class="card-text">
                    <li>Tech Support Cooperative
                        <ul>
                            <li><a href="https://www.core-pos.com/">CORE-POS</a></li>
                            <li><a href="https://site.techsupport.coop/">Tech Support Cooperative</a></li>
                            <li><a href="https://github.com/CORESupport">GitHub TTSC</a></li>
                            <li><a href="https://github.com/CORE-POS/IS4C">GitHub CORE-POS</a></li>
                        </ul>
                    </li>
                    <li>TCCP Presentations
                        <ul>
                            <li><a href="https://docs.google.com/presentation/d/1iIT5IoOEGY3gVgEzTjS4wXeYz6NC8StL1rsShFa2LeU/edit#slide=id.g742e3e7cd_1_33">Catering</a></li>
                            <li><a href="https://docs.google.com/presentation/d/16D7TPyWcwInkWsJlPCkOnN7P2Xem61Ig5h124EBTUY0/edit#slide=id.g3852b33b01_0_23">Taura CMS</a></li>
                        </ul>
                    </li>
                    <hr/>
                    <li><a href="https://bitbucket.org/jdpurdyvi/isthisfood">Bitbucket</a></li>
                    <li><a href="https://trello.com/b/RIViTptG/isthisfood#">Trello</a></li>
                </ul>
            </div>
            <div class="card-footer">
            </div>
        </div>            
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Projects</h5>
                <ul class="card-text">
                    <li><a href="https://getbootstrap.com">Bootstrap</a></li>
                    <li><a href="http://www.chartjs.org">Chart.js</a></li>
                    <li><a href="https://datatables.net">DataTables</a></li>
                    <li><a href="http://momentjs.com">Moment.js</a></li>
                    <li><a href="https://developer.mozilla.org">MDN Web Docs</a></li>
                    <li><a href="https://www.data.gov/">Data.gov Open Data</a></li>
                </ul>
            </div>
            <div class="card-footer">
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Technologies</h5>
                <ul class="card-text">
                    <li><a href="https://www.nearlyfreespeech.net/">Nearly Free Speech</a></li>
                    <li><a href="http://php.net/manual">PHP</a></li>
                    <li><a href="https://dev.mysql.com/doc/">MySQL</a></li>
                </ul>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>

<?php
/*
 * Load Data from MySQL instance
 * Apply filters
 * Return JSON data
 */

/*
 * TODO / Laundy List
 * Error Handling
 * Refactor as Class & Autoload
 */

    if (!empty($_REQUEST['a']) && $_REQUEST['a']=='user-run') {
        $param['date-from'] = strftime("%F", strtotime($_REQUEST['date-from']));
        // Expand this, throw error
            if ($param['date-from'] < '2016-01-01') {
                $param['date-from'] = '2016-01-01';
            }
        $param['date-until'] = strftime("%F", strtotime($_REQUEST['date-until']));
            if ($param['date-until'] > '2018-03-31' || $param['date-until'] < '2016-01-01') {
                $param['date-until'] = '2018-03-31';
            }
        $param['origin'] = 'ALL';
        $param['type'] = 'ALL';

    } else {
        $param['date-from'] = '2016-01-01';
        $param['date-until'] = '2016-12-31';
        $param['origin'] = 'ALL';
        $param['type'] = 'ALL';
        
    }

    require_once('isthisfood.config.php');

    try {
        $link = new PDO("mysql:host=isthisfood.db;dbname=isthisfood;", ISTHISFOOD_MySQL_USER, ISTHISFOOD_MySQL_PASSWORD);

        $query['select'] = '';
        $query['from'] = '';
        $query['where'] = '
    AND `ReportPeriod` >= '.$link->quote($param['date-from']).'
    AND `ReportPeriod` <= '.$link->quote($param['date-until']);
        $query['groupby'] = '';
        
        $sql = '
SELECT 
    `ReportPeriod`,
    /* `Arrival_Departure`,
    `Domestic_International`,
    `CargoType`, */
    SUM(`AirCargoTons`) AS \'AirCargoTons\'
FROM `Los_Angeles_International_Airport_-_Air_Cargo_Volume`
WHERE 1=1
    '.$query['where'].'
GROUP BY `ReportPeriod`
ORDER BY `ReportPeriod`';
        $result = $link->query($sql);
        if ($result) {
            $resultSet = $result->fetchAll();
            foreach ($resultSet as $row) {
                $chart['data'][] = array('ReportPeriod'=>$row['ReportPeriod'], 'AirCargoTons' => $row['AirCargoTons']);
            }
            $chart['json'] = json_encode($chart['data']);
            $report['data'] = $chart['data'];
        } else {
            echo 'SQL failed: ';
        }
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }

    $html['form'] = '
<div class="card w-100">
    <div class="card-header">
        <h4>Report Options</h4>
    </div>
    <div class="card-body">
        <form action="/" method="get" name="report-cargo-view">
            <input type="hidden" name="a" value="user-run"/>
            <div class="form-group row">
                <label for="report-date-from" class="col-sm-4 col-form-label"><span class="accesskey">D</span>ate From</label>
                <div class="col-sm-8">
                    <input accesskey="d" type="date" class="form-control" id="report-date-from" name="date-from" min="2016-01-01" max="2018-03-31" value="'.$param['date-from'].'"/>
                </div>
            </div>
            <div class="form-group row">
                <label for="report-date-until" class="col-sm-4 col-form-label">Date <span class="accesskey">U</span>ntil</label>
                <div class="col-sm-8">
                    <input accesskey="u" type="date" class="form-control" id="report-date-until" name="date-until" min="2016-01-01" max="2018-03-31" value="'.$param['date-until'].'">
                </div>
            </div>
            <div class="form-group row">
                <label for="report-origin" class="col-sm-4 col-form-label"><span class="accesskey">O</span>rigin</label>
                <div class="col-sm-8">
                    <select accesskey="o" class="form-control" disabled name="origin">
                        <option value="1">All</option>
                        <option value="2">Domestic</option>
                        <option value="3">International</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="report-origin" class="col-sm-4 col-form-label"><span class="accesskey">T</span>ype</label>
                <div class="col-sm-8">
                    <select accesskey="t" class="form-control" disabled name="type">
                        <option value="1">All</option>
                        <option value="2">Mail</option>
                        <option value="3">Freight</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-4">
                </div>
                <div class="col-sm-8 text-right">
                    <button accesskey="e" type="reset" class="btn btn-warning">R<span class="accesskey">e</span>set Form</button>
                    <button accesskey="r" type="submit" class="btn btn-primary">Query <span class="accesskey">R</span>eport</button>
                </div>
            </div>
        </form>
    </div>
</div>';

    $html['table'] = '
<div class="card w-100">
    <div class="card-header">
        <h4>Report Details</h4>
    </div>
    <div class="card-body">    
        <table class="table table-bordered table-sm" id="report-table">
            <caption>Summary Table Showing Report Data</caption>
            <thead>
                <tr>
                    <th scope="col">Month</th>
                    <th scope="col">Volume</th>
                </tr>
            </thead>
            <tbody>';
            $sum_AirCargoTons = 0;
            foreach ($report['data'] as $row) {
                $sum_AirCargoTons += $row['AirCargoTons'];
                $html['table'].='
                <tr>
                    <td>'.strftime("%Y-%m", strtotime($row['ReportPeriod'])).'</td>
                    <td class="text-right">'.$row['AirCargoTons'].'</td>
                </tr>';
            }
            $html['table'] .='
            </tbody>
            <tfoot>
                <tr class="table-info">
                    <td>Summary</td>
                    <td class="text-right">'.$sum_AirCargoTons.'</td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>';